﻿namespace Google_sheetAndro.Services
{
    public class GoogleEmail
    {
        public string id { get; set; }
        public string email { get; set; }
        public bool verified_email { get; set; }
        public string picture { get; set; }
    }
}
